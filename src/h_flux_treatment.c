/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: h_flux_treatment.c
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
// #include <curses.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include <time_series.h>
#include "SEB.h"

///////////////////////////////////////////////////
/*** Total thermal transfer on a unity surface ***/
///////////////////////////////////////////////////

double SEB_H_flux_tot(s_carac_seb *pshf, int i, char *name) // SW 04/05/2021 introduce pshf to store H_SW, H_lW, H_lH, H_sh which are used later for energy balance
{
  double H_sw, H_lw, H_lh, H_sh;
  s_input_seb *pH_inputs;

  pH_inputs = pshf->pH_inputs;

  // SW 01/04/2022 check the water temperature
  // if Tw < 0 �C or Tw > 35 �C, Tw is corrected by 15 �C
  if (pH_inputs->Tw < T_0) {
    pH_inputs->Tw = T_0;
  } else if (pH_inputs->Tw > T_LIMIT_UP) {
    pH_inputs->Tw = T_LIMIT_UP;
  }

  H_sw = SEB_H_flux_sw(pH_inputs->meteo[H_SW], pH_inputs->r_side[VTS]);
  pshf->pH_budget[FLUX_SW] = H_sw; // SW 04/05/2021 unit J/m2/s
  // printf("sw %lf ",H_sw);
  H_lw = SEB_H_flux_lw(pH_inputs->meteo[H_LW], pH_inputs->r_side[VTS], pH_inputs->Tw, pH_inputs->meteo[T_AIR]);
  pshf->pH_budget[FLUX_LW] = H_lw; // SW 04/05/2021 unit J/m2/s
  // printf("lw %lf ",H_lw);
  H_lh = SEB_H_flux_lh2(pH_inputs->Tw, pH_inputs->meteo[T_AIR], pH_inputs->meteo[HUMID], pH_inputs->meteo[U_WIND]);
  pshf->pH_budget[FLUX_LH] = H_lh; // SW 04/05/2021 unit J/m2/s
  // printf("lh %lf ",H_lh);
  H_sh = SEB_H_flux_sh2(pH_inputs->Tw, pH_inputs->meteo[T_AIR], pH_inputs->meteo[HUMID], pH_inputs->meteo[P_ATM], H_lh);
  pshf->pH_budget[FLUX_SH] = H_sh; // SW 04/05/2021 unit J/m2/s
  // printf("sh %lf\n",H_sh);

  return H_sw + H_lw + H_lh + H_sh;
}
///////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////
/*** Building the vector containing the total heat flux per cell ***/
/////////////////////////////////////////////////////////////////////

void SEB_H_flux_vector_building(long n_cell, s_carac_seb **p_surf_heat_flux) {
  long i;
  int nthreads, tid, chunk;

  chunk = CHUNKSIZE;
#pragma omp parallel shared(n_cell, p_surf_heat_flux, nthreads, chunk) private(i, tid)
  {
    tid = omp_get_thread_num();
    if (tid == 0) {
      nthreads = omp_get_num_threads();
      printf("Number of threads = %d\n", nthreads);
    }
    printf("Thread %d starting...\n", tid);

#pragma omp for schedule(dynamic, chunk)
    for (i = 0; i < n_cell; i++) {
      // p_surf_heat_flux[i]->H_flux = SEB_H_flux_tot(p_surf_heat_flux[i]->pH_inputs); // SW 25/01/2021 to activate if using this function
      printf("Thread %d: Heat_flux[%lu]= %lf\n", tid, i, p_surf_heat_flux[i]->H_flux);
    }

  } /* end of parallel section */
}
