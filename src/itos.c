/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/* Functions used to print the names of parameters
 * (transform an int into a string)
 */

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#ifdef OMP
#include <omp.h>
#endif

#include <time_series.h>
#include "SEB.h"

char *SEB_name_meteo(int answer) {
  char *name;

  switch (answer) {
  case T_AIR: {
    name = strdup("T_AIR");
    break;
  }
  case H_SW: {
    name = strdup("H_SW");
    break;
  }
  case H_LW: {
    name = strdup("H_LW");
    break;
  }
  case HUMID: {
    name = strdup("HUMID");
    break;
  }
  case P_ATM: {
    name = strdup("P_ATM");
    break;
  }
  case U_WIND: {
    name = strdup("U_WIND");
    break;
  }
  case T_SOIL: {
    name = strdup("T_SOIL");
    break;
  }
  default: {
    name = strdup("DEFAULT, unknown type of answer in itos.c");
    break;
  }
  }

  return name;
}

/*char *name_safran(int answer)
{
  char *name;

  switch(answer){
  case SAF : {name = strdup("Safran");break;}
  default : {print_error("ERROR, unknown type %d in name_safran() in itos.c",answer);break;;break;}
  }

  return name;
}
*/
