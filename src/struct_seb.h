/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: struct_seb.h
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#ifndef _STRUCT_SEB_H_
#define _STRUCT_SEB_H_

typedef struct surf_heat_flux s_carac_seb; // DK 09 05 2022 This should be s_heat_seb;
typedef struct heat_flux_input s_input_seb;

typedef struct reach_to_safran s_rts;
typedef struct reach_id id_reach;
typedef struct input_atm_var s_met;
typedef struct reach_input s_rs_input;
typedef struct meteo_data_set s_meteodata_seb; // DK 09  This should be s_carac_seb;

/********************************/

struct surf_heat_flux {
  long id_cell;
  s_input_seb *pH_inputs;
  double pH_budget[NBUDGET];
  double H_flux;
};

/***************************************************/

struct heat_flux_input { // All the input variables
  double *meteo;         // Array: will be filled with s_met
  double Tw;             // Internal stream variable: water temperature
  double *r_side;        // Riverside variable, filled with s_rts
};

struct reach_to_safran { // From id_safran to id_reach
  long id_meteo;
  id_reach *pid_reach;

  /** SW 13/07/2021 add next and prev **/
  s_rts *prev;
  s_rts *next;
};

struct reach_id {
  char *amont;
  char *aval;
  int voie;
};

/* Structure to get the meteo data : very important ! */
struct input_atm_var {  // Atmospheric variables, store them at files' reading
  long id_meteo;        // Likely to be id_safran
  s_ft *pmeteo[NMETEO]; // Time series ranging over the times (days and hours) considered
};

struct reach_input {
  id_reach *pid_reach;
  double val[NRIVSIDE]; // This will have to put under the form of a temporal function -> s_ft !
};

struct meteo_data_set {
  int year0_meteo;

  /* Corresp id_meteo to id_reach */
  s_rts **p_rts; // links reaches to safran's cell

  /* Safran data */
  char folder[STRING_LENGTH_SEB];
  s_met **p_met; // Size corresponds to the number of the different meteo data sets being used -> usually, number of safran's cells
  s_input_seb **p_safran;
  long n_meteocell;
  int *id_index_met; // siez of nele (libhyd), store corresponding index in p_met for each ele HYD

  int **id_meteo; // list of meteo cells

  // View to sky parameter handling shading of sky DK 09
  double viewToSky; // DK 09 05 2022 View to sky (1-Shading); configurable view to sky factor  (1 = Full open sky)

  /* Time delineation for gathering the right time frame of meteo data */
  double *t_extrema; // This variable has been added, and is important!

  double *H_flux_ttc;
  s_carac_seb **p_surf_heat_flux;
};

#define new_meteodata_seb() ((s_meteodata_seb *)malloc(sizeof(s_meteodata_seb))) // SW 04/02/2021
#define new_rts_seb() (s_rts *)malloc(sizeof(s_rts))
#define new_reach_id() (id_reach *)malloc(sizeof(id_reach))

#endif
