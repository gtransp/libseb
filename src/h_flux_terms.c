/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: h_flux_terms.c
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
// #include <curses.h>
#include <string.h>
#include <math.h>
#include <time_series.h>
#include "SEB.h"

//////////////////////////////
/*** Short wave radiation ***/
//////////////////////////////

double SEB_H_flux_sw(double H_sw, double vts) {
  double Rsw;

  // printf("H_sw: %lf, vts: %lf\n",H_sw,vts);

  Rsw = ALBEDO_MEAN;
  return (1. - Rsw) * H_sw * vts;
}

/////////////////////////////
/*** Long wave radiation ***/
/////////////////////////////

double SEB_H_flux_lw(double Hlw, double vts, double Tw, double Ta) {
  double back_r, atm_r, landcover_r;
  double eps_w, eps_v;

  // printf("Hlw: %lf, Tw: %lf, Ta: %lf, vts: %lf\n",Hlw,Tw,Ta,vts);

  eps_w = 0.96; // stream emissivity
  eps_v = 0.97; // vegetation emissivity

  back_r = eps_w * SIGMA_SEB * pow(Tw, 4.);                   // back radiation
  atm_r = vts * Hlw;                                          // atmospheric radiation
  landcover_r = eps_v * (1. - vts) * SIGMA_SEB * pow(Ta, 4.); // land cover radiation

  return -back_r + atm_r + landcover_r;
}

/////////////////////////////////////////////
/*** Thermal transf. through Latent heat ***/
/////////////////////////////////////////////

double SEB_H_flux_lh(double Tw, double Ta, double Hum, double wind) {
  double Le;
  double es_Tw, es_Ta, ea_Ta;
  double f_wind, E;

  // printf("Tw: %lf, Ta: %lf, Hum: %lf, wind: %lf\n",Tw,Ta,Hum,wind);

  Le = pow(10., 6.) * (2.501 - 2.361 * pow(10., -3.) * (Tw - T_0));

  es_Tw = 0.61078 * exp(17.27 * (Tw - T_0) / (237.3 + Tw - T_0));
  es_Ta = 0.61078 * exp(17.27 * (Ta - T_0) / (237.3 + Ta - T_0));
  ea_Ta = Hum / 100. * es_Ta; // WMO, 2008

  f_wind = WIND_A + WIND_B * wind; // Shanahan, 1984
  E = f_wind * (es_Tw - ea_Ta);    // Dingman, 2008

  return RHO_WATER_SEB * Le * (-E);
}

double SEB_H_flux_lh2(double Tw, double Ta, double Hum, double wind) {
  double es_Tw, es_Ta, ea_Ta;
  double f_wind;

  // printf("Tw: %lf, Ta: %lf, Hum: %lf, wind: %lf\n",Tw,Ta,Hum,wind);

  es_Tw = 0.61078 * exp(17.27 * (Tw - T_0) / (237.3 + Tw - T_0));
  es_Ta = 0.61078 * exp(17.27 * (Ta - T_0) / (237.3 + Ta - T_0));
  ea_Ta = Hum / 100. * es_Ta; // WMO, 2008

  f_wind = WIND2_A + WIND2_B * wind; // Leach and Moore, 2010

  return 285.9 * f_wind * (ea_Ta - es_Tw); // Magnusson et al., 2012
}

///////////////////////////////////////////////
/*** Thermal transf. through sensible heat ***/
///////////////////////////////////////////////

double SEB_H_flux_sh(double Tw, double Ta, double Hum, double wind, double Pa) {
  double Le, f_wind;

  // printf("Tw: %lf, Ta: %lf, Hum: %lf, wind: %lf, Pa: %lf\n",Tw,Ta,Hum,wind,Pa);

  Le = pow(10., 6.) * (2.501 - 2.361 * pow(10., -3.) * (Tw - T_0));
  f_wind = WIND_A + WIND_B * wind;

  return RHO_WATER_SEB * Le * f_wind * C_B * (Pa / P_REF) * (Ta - Tw);
}

double SEB_H_flux_sh2(double Tw, double Ta, double Hum, double Pa, double Le) {
  double Bowen, Pref;
  double es_Tw, es_Ta, ea_Ta;

  // printf("Tw: %lf, Ta: %lf, wind: %lf\n",Tw,Ta,wind);

  Pa = Pa / 1000.; // kPa -> Magnusson et al., 2012
  Pref = P_REF / 100.;
  es_Tw = 0.61078 * exp(17.27 * (Tw - T_0) / (237.3 + Tw - T_0));
  es_Ta = 0.61078 * exp(17.27 * (Ta - T_0) / (237.3 + Ta - T_0));
  ea_Ta = Hum / 100. * es_Ta;
  Bowen = C_B * (Pa / Pref) * (Tw - Ta) / (es_Tw - ea_Ta);

  return Bowen * Le;
}
