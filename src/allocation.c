/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: allocation.c
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
// #include <curses.h>
#include <string.h>
#include <math.h>
#include <time_series.h>
#include "SEB.h"

s_carac_seb **SEB_alloc_shf(long n_cell) {
  long i;
  s_carac_seb **p_shf;
  p_shf = (s_carac_seb **)malloc(sizeof(s_carac_seb *) * n_cell);

  for (i = 0; i < n_cell; i++) {
    p_shf[i] = (s_carac_seb *)malloc(sizeof(s_carac_seb));
    p_shf[i]->pH_inputs = (s_input_seb *)malloc(sizeof(s_input_seb));
  }

  return p_shf;
}

s_carac_seb **SEB_alloc_shf_full(long n_cell) // when the dimensions of "meteo" and "rside" are required..
{
  long i;
  s_carac_seb **p_shf;
  p_shf = (s_carac_seb **)malloc(sizeof(s_carac_seb *) * n_cell);

  for (i = 0; i < n_cell; i++) {
    p_shf[i] = (s_carac_seb *)malloc(sizeof(s_carac_seb));
    p_shf[i]->pH_inputs = (s_input_seb *)malloc(sizeof(s_input_seb));
    p_shf[i]->pH_inputs->meteo = (double *)malloc(sizeof(double) * NMETEO);
    p_shf[i]->pH_inputs->r_side = (double *)malloc(sizeof(double) * NRIVSIDE);
  }

  return p_shf;
}

s_met **SEB_alloc_met(long n_meteo) // struct for storing and distributing the meteo input data
{
  long i;
  s_met **p_met;
  p_met = (s_met **)malloc(sizeof(s_met *) * n_meteo);

  for (i = 0; i < n_meteo; i++)
    p_met[i] = (s_met *)malloc(sizeof(s_met));

  return p_met;
}

s_rts **SEB_alloc_rts(long n_reach) // reach to safran (correspondence)
{
  long i;
  s_rts **p_rts;
  p_rts = (s_rts **)malloc(sizeof(s_rts *) * n_reach);

  for (i = 0; i < n_reach; i++) {
    p_rts[i] = (s_rts *)malloc(sizeof(s_rts));
    p_rts[i]->pid_reach = (id_reach *)malloc(sizeof(id_reach));
    p_rts[i]->pid_reach->amont = (char *)malloc(sizeof(char) * STRING_LENGTH_SEB); // SW 10/03/2021 use STRING_LENGTH_SEB instead 50
    // p_rts[i]->pid_reach->aval = (char *)malloc(sizeof(char)*int); // SW 10/03/2021 need to define int
    // p_rts[i]->pid_reach->amont = (char *)malloc(sizeof(char)*STRING_LENGTH_SEB); // SW 10/03/2021 use STRING_LENGTH_SEB instead 50
    p_rts[i]->pid_reach->aval = (char *)malloc(sizeof(char) * STRING_LENGTH_SEB); // SW 19/03/2021 aval ?
  }

  return p_rts;
}

s_rs_input **SEB_alloc_rs_input(long n_reach) {
  long i;
  s_rs_input **p_reaches;
  p_reaches = (s_rs_input **)malloc(sizeof(s_rs_input *) * n_reach);

  for (i = 0; i < n_reach; i++) {
    p_reaches[i] = (s_rs_input *)malloc(sizeof(s_rs_input));
    p_reaches[i]->pid_reach = (id_reach *)malloc(sizeof(id_reach));
    p_reaches[i]->pid_reach->amont = (char *)malloc(sizeof(char) * STRING_LENGTH_SEB); // SW 19/03/2021 use STRING_LENGTH_SEB instead 50
    p_reaches[i]->pid_reach->aval = (char *)malloc(sizeof(char) * STRING_LENGTH_SEB);  // SW 19/03/2021 use STRING_LENGTH_SEB instead 50
  }

  return p_reaches;
}

/*** SW 13/07/2021 ***/

s_rts *SEB_create_rts(char *amont, char *aval, int voie, long id_meteo) {
  s_rts *prts;
  prts = new_rts_seb();
  prts->pid_reach = new_reach_id();

  prts->pid_reach->amont = amont;
  prts->pid_reach->aval = aval;
  prts->pid_reach->voie = voie;

  prts->id_meteo = id_meteo;

  return prts;
}

/** \fn s_rts *SEB_chain_bck_rts(s_rts *prts1, s_rts *prts2)
 * \brief Links two elements with structure s_rts  bck, ie usefull for yacc for undetermined list
 *
 * Read the pile from top (end of the list) to bottom (beginning of the list), but return a pointer to the beginning of the list as a first element
 */

s_rts *SEB_chain_bck_rts(s_rts *prts1, s_rts *prts2) {
  prts1->next = prts2;
  prts2->prev = prts1;

  return prts1;
}
