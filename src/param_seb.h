/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: param_seb.h
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// Version number of the present library
#define VERSION_SEB 0.09

#define STRING_LENGTH_SEB 300
// Parameters for heat flux term calculation:

// Shortwave mean reflection coefficient
#define ALBEDO_MEAN 0.075 // depending on the studies, value comprised between 0.05 and 0.1 for water

// Stefan-Boltzmann constant
#define SIGMA_SEB 0.00000005670367

// Gas constant for water vapour
#define R_V 461.5

// 0°C to Kelvin
#define T_0 273.15

// upper limit water temperature
#define T_LIMIT_UP 308.15
#define T_DEFAULT 288.15

// Coefficients defined according to Dunne and Leopold, 1978
#define WIND_A 0.00000001505
#define WIND_B 0.000000016
// Other wind coefficients, seen in Magnusson et al. 2012
#define WIND2_A 0.132
#define WIND2_B 0.143

#define C_B 0.0651

// Reference atmospheric pressure at sea level
#define P_REF 101325.

// Parallel computing param.
#define CHUNKSIZE 100

// Water density at 4°C and at right pressure conditions
#define RHO_WATER_SEB 1000.
// Air density at sea level roughly and 0°C..
#define RHO_AIR 1.292
// Heat capacity in the same conditions
#define C_P_AIR 1005.

#define VIEWTOWHISKY 0.8 // for test: constant view to sky param. (1 = totally clear)

enum ext_forcings { T_AIR, H_SW, H_LW, HUMID, P_ATM, U_WIND, T_SOIL, NMETEO };
enum side_forcings { VTS, NRIVSIDE };
enum budget_terms { FLUX_SW, FLUX_LW, FLUX_LH, FLUX_SH, NBUDGET }; // SW: short waves, LW: long waves, LH: latent heat, SH: sensible heat
enum id_name_seb { INTERN_SEB, GIS_SEB, ABS_SEB, NID_SEB };
