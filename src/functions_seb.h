/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: functions_seb.h
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#ifndef _FUNCTIONS_SEB_H_
#define _FUNCTIONS_SEB_H_

// h_flux_treatment.c
// double SEB_H_flux_tot(s_input_seb *);
double SEB_H_flux_tot(s_carac_seb *pshf, int, char *);
void SEB_H_flux_vector_building(long, s_carac_seb **);

// h_flux_terms.c
double SEB_H_flux_sw(double, double);
double SEB_H_flux_lw(double, double, double, double);
double SEB_H_flux_lh(double, double, double, double);
double SEB_H_flux_lh2(double, double, double, double);
double SEB_H_flux_sh(double, double, double, double, double);
double SEB_H_flux_sh2(double, double, double, double, double);

// allocation.c
s_carac_seb **SEB_alloc_shf(long);
s_carac_seb **SEB_alloc_shf_full(long);
s_met **SEB_alloc_met(long);
s_rts **SEB_alloc_rts(long);
s_rs_input **SEB_alloc_rs_input(long);
s_rts *SEB_create_rts(char *, char *, int, long);
s_rts *SEB_chain_bck_rts(s_rts *, s_rts *);

// dealloc.c
void SEB_dealloc_shf(long, s_carac_seb **);
void SEB_dealloc_shf_full(long, s_carac_seb **);

// itos.c
char *SEB_name_meteo(int);

// manage_mto_data.c
s_meteodata_seb *SEB_init_meteodata();
#endif
