/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: manage_mto_data.c
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
// #include <curses.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include <time_series.h>
#include "SEB.h"

s_meteodata_seb *SEB_init_meteodata() {
  s_meteodata_seb *pmto_data;

  pmto_data = new_meteodata_seb();
  bzero((s_meteodata_seb *)pmto_data, sizeof(s_meteodata_seb));

  pmto_data->year0_meteo = INITIAL_YEAR_JULIAN_DAY_TS; // 1850 default
  pmto_data->n_meteocell = 0;
  pmto_data->viewToSky = VIEWTOWHISKY; // DK 09 05 2022 Addition of viewtosky
  return pmto_data;
}
