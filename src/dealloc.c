/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libseb
 * FILE NAME: dealloc.c
 *
 * CONTRIBUTORS: Aurélien BORDET, Shuaitao WANG
 *
 * LIBRARY BRIEF DESCRIPTION: Atmosphere-water energy balance calculations.
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libseb Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
// #include <curses.h>
#include <string.h>
#include <math.h>
#include <time_series.h>
#include "SEB.h"

void SEB_dealloc_shf(long n_cell, s_carac_seb **p_shf) {
  long i;

  for (i = 0; i < n_cell; i++) {
    free(p_shf[i]->pH_inputs);
    free(p_shf[i]);
  }

  free(p_shf);
}

void SEB_dealloc_shf_full(long n_cell, s_carac_seb **p_shf) {
  long i;

  for (i = 0; i < n_cell; i++) {
    free(p_shf[i]->pH_inputs->meteo);
    free(p_shf[i]->pH_inputs->r_side);
    free(p_shf[i]->pH_inputs);
    free(p_shf[i]);
  }

  free(p_shf);
}
